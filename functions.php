<?php
function child_twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Sidebar Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Area', 'twentyten' ),
		'id' => 'footer-widget-area',
		'description' => __( 'These widgets will show up on your footer', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Slideshow Form', 'twentyten' ),
		'id' => 'slide-widget-area',
		'description' => __( 'These widgets will show up on your footer', 'twentyten' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
//AFTER THEME IS SET UP (FUNCTIONS.PHP IS LOADED FROM PARENT THEME) - REMOVE THE SIDEBAR WIDGETS
add_action('after_setup_theme','remove_parent_widgets');
function remove_parent_widgets() {
    remove_action( 'widgets_init', 'twentyten_widgets_init' );
}
//RE-ADD THE SIDEBAR WIDGETS WITH THE NEW CODE ADDED, DUPLICATING THE FUNCTION
add_action( 'after_setup_theme', 'child_twentyten_widgets_init' );

//CHANGE DEFAULT THEME NAME
add_filter('default_page_template_title', function() {
    return __('One column, right sidebar', 'your_text_domain');
});

//REMOVES HENTRY FROM PAGES TO ELIMINATE ERRORS IN GOOGLE WEBMASTER TOOLS
function themeslug_remove_hentry( $classes ) {
    if ( is_page() ) {
        $classes = array_diff( $classes, array( 'hentry' ) );
    }
    return $classes;
}
add_filter( 'post_class','themeslug_remove_hentry' );

//REMOVES QUERY(?) STRINGS FROM URLS
function _remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//DONT ERASE SPAN TAGS WHEN SWITCHING TO VISUAL / TEXT MODE
function override_mce_options($initArray) {
$opts = '*[*]';
$initArray['valid_elements'] = $opts;
$initArray['extended_valid_elements'] = $opts;
return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');


// BLOG ARCHIVE EXCERPT STYLE

function change_blog_archive_style() {
	global $avia_config;
	if(!is_single()){ $avia_config['blog_content'] = "excerpt_read_more"; }
}
add_action('get_header', 'change_blog_archive_style');

// Scripts and stylesheets
function atc_styles_scrpits(){
		// Fonts
		wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', false );
		wp_enqueue_style( 'act-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false );
}
add_action('wp_enqueue_scripts', 'atc_styles_scrpits');

?>
