<?php 

$sliderpages = array(
	'123' => 419, // Residential Strata Properties
	'141' => 417, // Cedar Fencing
	'148' => 627, // Yard Clean Up
	'19'  => 420, // Commercial Businesses
	'21'  => 418, // Residential Home Owners
	'8'   => 547, // About Page
	'139' => 537, // Lawn & Garden
	'146' => 530, // Hedges & Trees
	'12'  => 553, // Testimonials
	'14'  => 555, // Testimonials
);

$pageId = get_the_ID();
if ($pageId) {
	if (array_key_exists($pageId, $sliderpages)) {
		echo do_shortcode("[metaslider id=$sliderpages[$pageId]]");
	} elseif (is_front_page()) { 
		// echo do_shortcode("[masterslider id='2']");
		echo do_shortcode("[metaslider id=37]");
	} elseif (is_home()) {
		echo do_shortcode("[metaslider id=554]");// Blog
	} else{
		echo do_shortcode("[metaslider id=627]");
	}
}




?>