<nav role="navigation" class="slider-nav">
	<?php 
		
		$slidernavmenu = array(
		 'container_class' => 'menu-header',
		 'theme_location'  => 'primary'
		);

		wp_nav_menu( $slidernavmenu );   
	?>
</nav>