<?php
/**
 * The Header for our child theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Responsive Twenty_Ten
 * @since Responsive Twenty Ten 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<!-- <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" /> -->
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?version=1.1" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">  -->
<!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet"> -->

<!-- <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css"> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->

<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
<script src="https://google.com/recaptcha/api.js?render=6LfsvpAUAAAAAOz4XkHipY2wl6BllOkPeXPzkyLt" async defer></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header class="fullwidth" role="banner">
	<div class="container-header">
		
		<div class="left">
			<div class="logo-cont">
				<div class="vcenter">
					<a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					
					<?php if(is_front_page()): ?>
						<h1><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></h1>
					<?php endif; ?>
					
					<img class="logo" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/atc-logo-reverse.png" />
				</a>
				</div>
			</div>
			<div class="site-brand">
				<div class="vcenter">
					<div>
						<h2>Landscape Maintenance &amp; <br> Gardening Services</h2>
						<p>Your local, professional and eco-friendly landscape experts. <br>On time. As Quoted. That’s the ATC  Landscape Guarantee.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="right">
			<div class="vcenter">
				<div class="cta-cont">
					<div class="cta-alpha">
						<a href="tel:604-720-2853" class="phone">
							<div class="header"><i class="fa fa-volume-control-phone" aria-hidden="true"></i></div>
							<div class="body">Give us a call <br> <strong>604&#8209;720&#8209;2853</strong></div>
						</a>
					</div>

					<div class="cta-alpha">
						<a href="mailto:info@atclandscape.com" class="email">
							<div class="header"><i class="fa fa-envelope-open-o" aria-hidden="true"></i></div>
							<div class="body">Send us an email <strong>info@atclandscape.com</strong></div>
						</a>
					</div>
				</div>
			</div>
		</div>

	</div>  
</header>

<div class="topbar">
	<div class="top-container">
		<div class="left"><div class="inline"><?php echo do_shortcode( '[listmenu menu="top menu" menu_id="top_menu"]' ); ?></div></div>
		<div class="right"> <div class="inline"><?php echo do_shortcode( '[cn-social-icon attr_class="whitesocial" selected_icons="2,3,1,4,5"]' ); ?></div> </div>
	</div>
</div>

<?php // Nav, Slider and Lead Form ?>
<div id="sform">
	<div class="container bravo"> <?php get_template_part("/inc/slider-nav"); ?> </div>
	<?php get_template_part("/inc/slider"); ?>
	<div class="charlie"><?php get_template_part("/inc/slider-widget"); ?></div>
</div>


