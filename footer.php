<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<footer class="fullwidth" role="contentinfo">
	
	<div class="container"> <?php dynamic_sidebar( 'footer-widget-area' ); ?> </div>
	
	<div class="loclinks">
		<div class="container">
			<a href="/">Home</a> <a href="/vancouver-landscaper">About Coquitlam Landscapers</a> <a href="/landscapers-vancouver">Residential Strata Landscaping Coquitlam</a> <a href="/landscaping-vancouver" >Commercial Landscaping Coquitlam</a> <a href="/lawn-maintenance-vancouver">Lawn Maintenance Coquitlam</a><br>
			<a href="/lawn-maintenance-vancouver">Gardening Coquitlam</a> <a href="/cedar-fencing-vancouver">Cedar Fencing Coquitlam</a> <a href="/tree-services-vancouver">Hedge &amp; Tree Trimming Coquitlam</a> <a href="/tree-services-vancouver">Pruning Coquitlam</a> <a href="/vancouver-landscaping">Rubbish Removal Coquitlam</a> <a href="/contact-atc-landscape">Contact ATC Landscape</a>	
		</div>
	</div>	
		
	<section class="copywrap">
		
		<div class="container">

			<div id="copyright">  
				<?php get_sidebar( 'footer' ); $date = getdate(); $year = $date['year']; ?>
				Copyright &copy; <?php echo("$year"); ?> <a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
			</div>

			<div id="longevity">
				<a target="_blank" href="http://www.longevitygraphics.com">Website Design</a> by <a target="_blank" href="http://www.longevitygraphics.com">Longevity Graphics</a>
			</div>

		</div>
	</section>
</footer>




<?php wp_footer(); ?>
</body>
</html>